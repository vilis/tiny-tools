# Tiny Tools

## Description
Tiny Tools is an AI-driven web application crafted using Django for backend operations and Bootstrap for the frontend design.

Featured tools:
- AI Chatbot: A unique implementation of the OpenAI chatbot.
- Test Solver: A creative tool designed using the Google Cloud Vision API and OpenAI API to solve tests derived from images!

A user login is required to access these tools.

## Installation
1. Clone the repository:

<code>git clone https://gitlab.com/vilis/tiny-tools</code>

2. Install virtual environment
    
    Windows:
    - Open command prompt
    - Navigate to your project directory
    - Once inside the project directory, create a new virtual environment by running: <code> python -m venv venv</code>
    - Activate the virtual environment <code>.\myvenv\Scripts\activate</code>

    Linux/MacOS:
    - Open Terminal
    - Navigate to your project directory
    - Once inside the project directory, create a new virtual environment by running: <code>python3 -m venv venv</code>
    - Activate the virtual environment: <code> source myvenv/bin/activate </code>

3. Install packages from requirements.txt file
    - Make sure your virtual environment is activated. You can tell if your environment is active by looking at your command line prompt. If it starts with (venv), then your environment is active.
    - Use pip to install the packages: <code> pip install -r requirements.txt </code>

## License
Distributed under the MIT License.


