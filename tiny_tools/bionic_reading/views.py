from django.shortcuts import render

# Create your views here.

def bionic_index(request):
    return render(request, 'bionic_index.html')