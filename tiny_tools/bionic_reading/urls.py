from django.urls import path
from . import views

urlpatterns = [
    path('', views.bionic_index, name='bionic_index'),
]