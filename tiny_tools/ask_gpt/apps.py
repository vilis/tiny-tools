from django.apps import AppConfig


class AskGptConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ask_gpt'
