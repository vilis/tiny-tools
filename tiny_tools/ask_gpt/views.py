from django.shortcuts import render
from django.http import JsonResponse
from lens_gpt.utils.ocr import AskGPT
from .models import Chat
from django.utils import timezone


# Create your views here.
def ask_gpt(request):
    if request.user.is_authenticated:
        chats = Chat.objects.filter(user=request.user)
        if request.method == 'POST':
            message = request.POST.get('message')
            response = generate_response_to_question(message)

            chat = Chat(user=request.user, message=message, response=response, created_at=timezone.now())
            chat.save()
            
            return JsonResponse({'message' : message, 'response' : response})
        return render(request, 'ask_gpt_index.html', {'chats' : chats})
    else:
        return render(request, 'ask_gpt_index.html')

def generate_response_to_question(message):
    askgpt = AskGPT('lens_gpt/utils/gpt_key.json')
    response = askgpt.gpt_response(message)
    
    return response[1]