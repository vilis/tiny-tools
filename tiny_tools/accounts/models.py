# accounts/models.py
from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True, default='avatars/default_avatar.png')
    description = models.TextField(null=True, blank=True, default="Welcome to my profile!")

    def __str__(self):
        return self.username