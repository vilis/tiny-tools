# accounts/forms.py
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model

from .models import CustomUser
from django import forms


class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = ("username", "email", "password1", "password2")

        widgets = {
            'username': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter your username'}),
            'email': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter your email'}),
            'password1': forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Enter your password'}),
            'password2': forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirm your password'}),
            }


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = get_user_model()
        fields = ("username", "email")


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['avatar', 'description']
