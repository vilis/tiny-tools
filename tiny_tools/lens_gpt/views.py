from django.shortcuts import render
from django.views.generic import CreateView, DetailView

from .forms import PostForm  # new
from .models import Post

from django.urls import reverse_lazy
from django.urls import reverse


# Create your views here.
def lens_index(request):
    return render(request, 'lens_index.html')


class CreatePostView(CreateView):  # new
    model = Post
    form_class = PostForm
    template_name = "post.html"

    def get_success_url(self):
        return reverse('post_detail', args=[self.object.pk])


class PostDetailView(DetailView):
    model = Post
    template_name = "post_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
