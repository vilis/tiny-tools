from google.cloud import vision
from google.oauth2 import service_account
import re
import openai
import json

class AskGPT:
    def __init__(self, gpt_key_path) -> None:
        self.openai_api_key = self.load_gpt_key(gpt_key_path)
        openai.api_key = self.openai_api_key
        
    def load_gpt_key(self, key_path):
        with open(key_path) as config_file:
            config_data = json.load(config_file)
        return config_data["openai_api_key"]
    
    def gpt_response(self, question):
            chat_completion = openai.ChatCompletion.create(
                model="gpt-4", messages=[{"role": "user", "content": question}]
            )
            return question, chat_completion["choices"][0]["message"]["content"]

class ImageProcessor:
    def __init__(self, path, gpt_key_path, google_key_path):
        self.path = path
        self.openai_api_key = self.load_gpt_key(gpt_key_path)
        self.credentials = self.load_google_key(google_key_path)
        openai.api_key = self.openai_api_key
        
    def load_gpt_key(self, key_path):
        with open(key_path) as config_file:
            config_data = json.load(config_file)
        return config_data["openai_api_key"]

    def load_google_key(self, key_path):
        return service_account.Credentials.from_service_account_file(key_path)

    def gpt_response(self, question):
            chat_completion = openai.ChatCompletion.create(
                model="gpt-3.5-turbo", messages=[{"role": "user", "content": question}]
            )
            return chat_completion["choices"][0]["message"]["content"]
        
    def google_vision(self, img):
        client = vision.ImageAnnotatorClient(credentials=self.credentials)

        with open(img, "rb") as image_file:
            content = image_file.read()

        image = vision.Image(content=content)

        response = client.document_text_detection(image=image)
        document = response.full_text_annotation
        print(document.text)
        
        blocks = [
            (''.join([symbol.text for symbol in word.symbols]), block.bounding_box.vertices[0].y, block.bounding_box.vertices[0].x)
            for page in document.pages 
            for block in page.blocks 
            for para in block.paragraphs 
            for word in para.words
            ]

        blocks.sort(key=lambda x: (x[1], x[2]))

        document = " ".join(block[0] for block in blocks)


        return str(document)


    def find_numbers(self, text):
        return [(m.start(), m.group()) for m in re.finditer('\d+', text)]


    def extract_questions(self):
        text = self.google_vision(self.path)
        numbers = self.find_numbers(text)
        tasks = {
            'questions' : [],
            'answers' : []
        }
        
        question_no = 1

        pos = 0
        for number in numbers:
                #print(number)
                if number[1] == str(question_no) and number[1] == '1':
                    pos = number[0]
                    question_no += 1
                elif number[1] == str(question_no) and number[0] - pos > 10: 
                    # dodatkowy warunek sprawdzający odległość między obecnym i poprzednim numerem
                    tasks['questions'].append(text[pos:number[0]])
                    tasks['answers'].append(self.gpt_response(tasks['questions'][-1]))
                    pos = number[0]
                    question_no += 1

                if number == numbers[-1]:
                    tasks['questions'].append(text[pos:])
                    tasks['answers'].append(self.gpt_response(tasks['questions'][-1]))

        return tasks
        
        

    # if __name__ == "__main__":
    #     PATH = '/Users/vilis/desktop/tests/test1.jpg'
    #     doc = google_vision(PATH)
    #     numbers = find_numbers(doc)
    #     extract_questions
    
    

