from django.db import models
import os
from uuid import uuid4
from lens_gpt.utils.ocr import ImageProcessor
from django.conf import settings


class Post(models.Model):
    title = models.CharField(max_length=255, blank=True)
    image = models.ImageField(
        upload_to='images/', default="/user.png", null=True, blank=True)
    questions_and_answers = models.JSONField(blank=True, null=True)
    
    def save(self, *args, **kwargs):
        if self.image and not self.pk:
            ext = self.image.name.split('.')[-1]
            filename = f'{uuid4().hex}.{ext}'
            self.image.name = filename
            self.title = filename

        super(Post, self).save(*args, **kwargs)
        self.questions_and_answers = self.ask()
        super(Post, self).save(update_fields=['questions_and_answers'])


    def ask(self):
        current_directory = os.path.dirname(os.path.abspath(__file__))
        GPT_KEY_PATH = os.path.join(current_directory, 'utils/gpt_key.json')
        GOOGLE_KEY_PATH = os.path.join(current_directory, 'utils/google_key.json')
        
        img_path = self.image_path()
        img = ImageProcessor(img_path, GPT_KEY_PATH, GOOGLE_KEY_PATH)
        return img.extract_questions()

    def image_path(self):
        if self.image:
            return self.image.path
        else:
            return settings.STATIC_URL + 'user.png'

    def __str__(self):
        return self.title
