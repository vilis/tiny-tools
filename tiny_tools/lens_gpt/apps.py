from django.apps import AppConfig


class LensGptConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lens_gpt'
