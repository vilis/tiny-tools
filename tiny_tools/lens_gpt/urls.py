from django.urls import path
from . import views
from .views import CreatePostView, PostDetailView

urlpatterns = [
    path("", CreatePostView.as_view(), name="add_post"),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post_detail'),

]