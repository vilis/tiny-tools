from django.shortcuts import render

# def index(request):
#     return render(request, 'index.html')


def features(request):
    current_page = "features"
    return render(request, 'features.html', {'current_page' : current_page})

def contact(request):
    current_page = "contact"
    return render(request, 'contact.html', {'current_page' : current_page})

